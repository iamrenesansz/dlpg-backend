<?php namespace DLPG\Http\Controllers;

use DLPG\Models\License;

use DLPG\Http\Requests;
use DLPG\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AdminController extends Controller {


    private $license;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(License $license)
    {
        $this->middleware('auth');
        $this->license = $license;
    }

    public function index()
    {
        $query = '';

        if (\Input::get('query')) {
            $query = \Input::get('query');
        }

        $select = [
            'license_num',
            'image',
            \DB::raw('CONCAT(last_name, ", ", first_name, " ", middle_name) AS name'),
            \DB::raw('CONCAT(violation_category.title, ": ", violation.title) AS violation'),
            \DB::raw('DATE(tbl_licenses.created_at) as created_at')
        ];
        $results = $this->license
                        ->join('tbl_license_violations as license_violations', 'license_violations.license_id', '=', 'tbl_licenses.id')
                        ->join('tbl_violations as violation', 'violation.id', '=', 'license_violations.violation_id')
                        ->join('tbl_violation_categories as violation_category', 'violation_category.id', '=', 'violation.violation_cat_id')
                        ->where('license_num', 'LIKE', '%'.$query.'%')
                        ->orWhere('last_name', 'LIKE', '%'.$query.'%')
                        ->orWhere('first_name', 'LIKE', '%'.$query.'%')
                        ->orWhere('middle_name', 'LIKE', '%'.$query.'%')
                        ->get($select);
                        
        return view('home', compact('results'));
    }

}
