<?php namespace DLPG\Http\Controllers\License;

use DLPG\Models\License;
use DLPG\Http\Requests;
use DLPG\Http\Controllers\Controller;

use Illuminate\Http\Request;

class LicenseController extends Controller {

	/**
	 * License Model
	 * 
	 * @var License
	 */
	private $license;

	/**
	 * Create controller instance
	 *
	 * @param License $license
	 */
	public function __construct(License $license)
	{
        $this->middleware('auth');
		$this->license = $license;
	}

	/**
     * @access public
     * 
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $query = '';

        if (\Input::get('query')) {
            $query = \Input::get('query');
        }

		$results = $this->license
						->where('license_num', 'LIKE', '%'.$query.'%')
                        ->orWhere('last_name', 'LIKE', '%'.$query.'%')
                        ->orWhere('first_name', 'LIKE', '%'.$query.'%')
                        ->orWhere('middle_name', 'LIKE', '%'.$query.'%')
						->get();

		return view('license.index', compact('results'));
	}

	public function doUpdate($id)
	{
		$updated = true;
		$input = \Input::get('input');
		
		if(\Input::file('image')) {
			$image = \Input::file('image')->move(__DIR__.'../../../../../public/storage/',\Input::file('image')->getClientOriginalName());
			
			$input['image'] = $image->getFilename();
		}

		$results = $this->license->find($id);

		// Update value
		foreach ($input as $key => $value) {
			$results->$key = $input[$key];
		}

		$results->save();
		$results = $results->toArray();


		return view('license.edit', compact('results', 'updated'));
	}

	public function edit($id)
	{
		$results = $this->license->find($id)->toArray();
		$updated = false;

		return view('license.edit', compact('results', 'updated'));
	}

	public function create()
	{
		return view('license.add');
	}

	public function doCreate()
	{
		// Get $_POST variables
		$input = \Request::all();
		$image = $image = \Input::file('image')->move(__DIR__.'../../../../../public/storage/',\Input::file('image')->getClientOriginalName());;
		$input['image'] = $image->getFilename();

		$updated = false;

		unset($input['_token']);

		$newEntry = $this->license->firstOrCreate($input);

		return redirect()->route('get.license.edit', ['id' => $newEntry->id]);
	}

	/**
     * @access public
     * 
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$find = $this->license->find($id);
		$find->delete();

		return redirect()->route('get.license');
	}

}
