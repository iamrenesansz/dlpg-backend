<?php namespace DLPG\Http\Controllers\ViolationCategory;

use DLPG\Models\ViolationCategory;
use DLPG\Http\Requests;
use DLPG\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ViolationCategoryController extends Controller {

	/**
	 * ViolationCategory Model
	 * 
	 * @var ViolationCategory
	 */
	private $violationCategory;

	/**
	 * Create controller instance
	 *
	 * @param ViolationCategory $violationCategory
	 */
	public function __construct(ViolationCategory $violationCategory)
	{
        $this->middleware('auth');
		$this->violationCategory = $violationCategory;
	}

	/**
     * @access public
     * 
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $query = '';

        if (\Input::get('query')) {
            $query = \Input::get('query');
        }

		$results = $this->violationCategory->where('title', 'LIKE', '%'.$query.'%')->get();

		return view('violation-category.index', compact('results'));
	}

	public function doUpdate($id)
	{
		$updated = true;

		$input = \Input::get('input');
		$results = $this->violationCategory->find($id);

		// Update value
		foreach ($input as $key => $value) {
			$results->$key = $input[$key];
		}

		$results->save();
		$results = $results->toArray();


		return view('violation-category.edit', compact('results', 'updated'));
	}

	public function edit($id)
	{
		$results = $this->violationCategory->find($id)->toArray();
		$updated = false;

		return view('violation-category.edit', compact('results', 'updated'));
	}

	public function create()
	{
		return view('violation-category.add');
	}

	public function doCreate()
	{
		// Get $_POST variables
		$input = \Request::all();
		$updated = false;

		unset($input['_token']);

		$newEntry = $this->violationCategory->firstOrCreate($input);

		return redirect()->route('get.violation-category.edit', ['id' => $newEntry->id]);
	}

	/**
     * @access public
     * 
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$find = $this->violationCategory->find($id);
		$find->delete();

		return redirect()->route('get.violation-category');
	}

}
