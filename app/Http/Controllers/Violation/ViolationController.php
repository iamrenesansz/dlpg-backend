<?php namespace DLPG\Http\Controllers\Violation;

use DLPG\Models\Violation;
use DLPG\Models\ViolationCategory;

use DLPG\Http\Requests;
use DLPG\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ViolationController extends Controller {

    /**
     * Violation Model
     * 
     * @var Violation
     */
    private $violation;
    private $violationCategory;

    /**
     * Create controller instance
     *
     * @param Violation $violation
     */
    public function __construct(Violation $violation, ViolationCategory $violationCategory)
    {
        $this->middleware('auth');
        $this->violation = $violation;
        $this->violationCategory = $violationCategory;
    }

    /**
     * @access public
     * 
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $query = '';

        if (\Input::get('query')) {
            $query = \Input::get('query');
        }

        $select = [
            'tbl_violations.id',
            'tbl_violations.violation_cat_id',
            \DB::raw('tbl_violations.title AS violation'),
            'tbl_violations.min_rate',
            'tbl_violations.max_rate'
        ];

        $results = $this->violation
                           ->join('tbl_violation_categories AS violation_category', 'violation_category.id', '=', 'tbl_violations.violation_cat_id')
                           ->where('tbl_violations.title', 'LIKE', '%'.$query.'%')
                           ->get($select);

        return view('violation.index', compact('results'));
    }

    public function doUpdate($id)
    {
        $updated = true;

        $input = \Input::get('input');
        $results = $this->violation->find($id);
    	$violationCategories = $this->violationCategory->get();

        // Update value
        foreach ($input as $key => $value) {
            $results->$key = $input[$key];
        }

        $results->save();
        $results = $results->toArray();


        return view('violation.edit', compact('results', 'violationCategories', 'updated'));
    }

    public function edit($id)
    {
        $results = $this->violation->find($id)->toArray();
    	$violationCategories = $this->violationCategory->get();
        $updated = false;

        return view('violation.edit', compact('results', 'violationCategories', 'updated'));
    }

    public function create()
    {
    	$violationCategories = $this->violationCategory->get();

        return view('violation.add', compact('violationCategories'));
    }

    public function doCreate()
    {
        // Get $_POST variables
        $input = \Request::all();
        $updated = false;

        unset($input['_token']);

        $newEntry = $this->violation->firstOrCreate($input);

        return redirect()->route('get.violation.edit', ['id' => $newEntry->id]);
    }

    /**
     * @access public
     * 
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $find = $this->violation->find($id);
        $find->delete();

        return redirect()->route('get.violation');
    }

}
