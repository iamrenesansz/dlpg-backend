<?php namespace DLPG\Http\Controllers\API\v1;

use DLPG\Models\Officer;
use DLPG\Models\License;
use DLPG\Models\LicenseViolation;
use DLPG\Http\Requests;
use DLPG\Http\Controllers\Controller;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class LicenseController extends Controller {

	/**
	 * License Model
	 * 
	 * @var License
	 */
	private $license;

	/**
	 * Create controller instance
	 *
	 * @param License $license
	 */
	public function __construct(License $license)
	{
		$this->license = $license;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()		
	{
		return view('license.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$response = [
			'data' => null,
			'errors' => null,
			'success' => true,
			'code' => 200,
			'timestamp' => date('Y-m-d g:i:s A')
		];
		
		// Get $_POST variables
		$input = \Request::all();

		try {

			if (Officer::find(['id' => $input['officer_id'], 'session_token' => $input['session_token']])->count()) {
				
				$officerID = $input['officer_id'];
				$violationID = $input['violation_id'];

				unset($input['officer_id']);
				unset($input['violation_id']);
				unset($input['session_token']);

				// Store to database
				$newLicense = $this->license->firstOrCreate($input);

				$newLicenseViolation = new LicenseViolation;
				$newLicenseViolation->license_id = $newLicense->id;
				$newLicenseViolation->violation_id = $violationID;
				$newLicenseViolation->arresting_officer_id = $officerID;
				$newLicenseViolation->save();

				$response['data'] = [
					'licens_id' => $newLicense->id,
					'license_violation_id' => $newLicenseViolation->id
				];

			} else {
				$response['success'] = false;
				$response['errors'] = 'Invalid id/session token provided.';
				$response['code'] = 400;
			}

		} catch(QueryException $ex) {

			$response['success'] = false;
			$response['errors'] = $ex->errorInfo;
			$response['code'] = 400;

		}

		return json_encode($response);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id = null)
	{
		$response = [
			'data' => null,
			'success' => true,
			'code' => 200,
			'timestamp' => date('Y-m-d g:i:s A')
		];

		try {

			if ($id === null)
				$response['data'] = $this->license->get();
			else
				$response['data'] = $this->license->find($id);

		} catch(QueryException $ex) {
			$response['success'] = false;
			$response['code'] = 400;
		}

		return json_encode($response);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return json_encode(['method' => 'PUT', 'data' => \Input::all()]);
	}

}
