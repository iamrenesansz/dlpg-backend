<?php namespace DLPG\Http\Controllers\API\v1;

use DLPG\Models\Violation;
use DLPG\Http\Requests;
use DLPG\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ViolationController extends Controller {

	/**
	 * Violation Model
	 * 
	 * @var License
	 */
	private $violation;

	/**
	 * Create controller instance
	 *
	 * @param License $license
	 */
	public function __construct(Violation $violation)
	{
		$this->violation = $violation;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id = null)
	{
		$result = [
			'data' => null,
			'success' => true,
			'code' => 200,
			'timestamp' => date('Y-m-d g:i:s A')
		];
		$select = [
			'tbl_violations.id',
			\DB::raw('CONCAT(violation_category.title, ": ", tbl_violations.title) AS violation'),
			'tbl_violations.min_rate',
			'tbl_violations.max_rate'
		];

		if ($id === null) {
			$result['data'] = $this->violation
								   ->join('tbl_violation_categories AS violation_category', 'violation_category.id', '=', 'tbl_violations.violation_cat_id')
								   ->get($select);
		}
		else {
			$result['data'] = $this->violation
								   ->join('tbl_violation_categories AS violation_category', 'violation_category.id', '=', 'tbl_violations.violation_cat_id')
								   ->find($id, $select);
		}
		
		return json_encode($result);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
