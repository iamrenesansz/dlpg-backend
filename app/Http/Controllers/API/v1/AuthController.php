<?php namespace DLPG\Http\Controllers\API\v1;

use DLPG\Models\Officer;
use DLPG\Models\OfficerDevice;
use DLPG\Http\Requests;
use DLPG\Http\Controllers\Controller;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use Sinergi\Token\StringGenerator;

class AuthController extends Controller {

    /**
     * Officer Model
     * 
     * @var License
     */
    private $officer;

    /**
     * Create controller instance
     *
     * @param License $license
     */
    public function __construct(Officer $officer)
    {
        $this->$officer = $officer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function login()
    {
        $response = [
            'data' => null,
            'errors' => null,
            'success' => true,
            'code' => 200,
            'timestamp' => date('Y-m-d g:i:s A')
        ];

        // Get $_POST Input
        $input = \Request::all();

        try {

            if (Officer::where('control_num', '=', $input['control_num'])->count()) {
                $officerDevice = OfficerDevice::firstOrNew(['device_id' => $input['device_id']]);
                $officer = Officer::where('control_num', '=', $input['control_num'])->first();
                $token = \Hash::make(StringGenerator::randomAlnum(100));

                // Save officer device ID
                $officerDevice->officer_id = $officer->id;
                $officerDevice->device_id = $input['device_id'];
                $officerDevice->save();

                // Update session_token
                $officer->session_token = $token;
                $officer->save();

                $response['data'] = $officer;
            } else {
                $response['errors'] = 'Login error: Control number is not registered.';
                $response['success'] = false;
                $response['code'] = 400;
            }

        } catch(\Exception $ex) {
            
            $response['success'] = false;
            $response['errors'] = 'API Error: An error has occured.';
            $response['code'] = 400;

        }

        return json_encode($response);
    }

}
