<?php namespace DLPG\Http\Controllers\Officer;

use DLPG\Models\Officer;
use DLPG\Http\Requests;
use DLPG\Http\Controllers\Controller;

use Illuminate\Http\Request;

class OfficerController extends Controller {

	/**
	 * Officer Model
	 * 
	 * @var Officer
	 */
	private $officer;

	/**
	 * Create controller instance
	 *
	 * @param Officer $officer
	 */
	public function __construct(Officer $officer)
	{
        $this->middleware('auth');
		$this->officer = $officer;
	}

	/**
     * @access public
     * 
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $query = '';

        if (\Input::get('query')) {
            $query = \Input::get('query');
        }

		$results = $this->officer
                        ->where('control_num', 'LIKE', '%'.$query.'%')
                        ->orWhere('last_name', 'LIKE', '%'.$query.'%')
                        ->orWhere('first_name', 'LIKE', '%'.$query.'%')
						->get();

		return view('officer.index', compact('results'));
	}

	public function doUpdate($id)
	{
		$updated = true;

		$input = \Input::get('input');
		$results = $this->officer->find($id);

		// Update value
		foreach ($input as $key => $value) {
			$results->$key = $input[$key];
		}

		$results->save();
		$results = $results->toArray();


		return view('officer.edit', compact('results', 'updated'));
	}

	public function edit($id)
	{
		$results = $this->officer->find($id)->toArray();
		$updated = false;

		return view('officer.edit', compact('results', 'updated'));
	}

	public function create()
	{
		return view('officer.add');
	}

	public function doCreate()
	{
		// Get $_POST variables
		$input = \Request::all();
		$updated = false;

		unset($input['_token']);

		$newEntry = $this->officer->firstOrCreate($input);

		return redirect()->route('get.officer.edit', ['id' => $newEntry->id]);
	}

	/**
     * @access public
     * 
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$find = $this->officer->find($id);
		$find->delete();

		return redirect()->route('get.officer');
	}

}
