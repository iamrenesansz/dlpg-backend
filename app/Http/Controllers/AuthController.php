<?php namespace DLPG\Http\Controllers;

use DLPG\Http\Requests;
use DLPG\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AuthController extends Controller {

    public function __construct()
    {
        if (\Auth::check()) {
        	return redirect()->route('get.admin');
        }
    }

    public function showLogin()
    {
        return view('auth.login');
    }

    public function doLogout()
    {
        \Auth::logout(); // log the user out of our application
        return redirect()->route('get.admin.login'); // redirect the user to the login screen
        // return Redirect::guest(); // redirect the user to the login screen
    }

	public function doLogin()
    {
        // validate the info, create rules for the inputs
        $rules = array(
            'username'    => 'required|alphaNum', // make sure the username is an actual username
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = \Validator::make(\Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return \Redirect::to('admin/login')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(\Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'username'     => \Input::get('username'),
                'password'  => \Input::get('password')
            );

            // attempt to do the login
            if (\Auth::attempt($userdata)) {

                // validation successful!
                // redirect them to the secure section or whatever
                // return \Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)
                return redirect()->route('get.admin'); // redirect the user to the login screen

            } else {        

                // validation not successful, send back to form 
                return redirect()->route('get.admin.login'); // redirect the user to the login screen

            }

        }
    }


}
