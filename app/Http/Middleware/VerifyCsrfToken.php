<?php namespace DLPG\Http\Middleware;

use Closure;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

    public function handle($request, Closure $next)
    {
        if ($this->isReading($request) || $this->excludedRoutes($request) || $this->tokensMatch($request))
            return $this->addCookieToResponse($request, $next($request));

        throw new TokenMismatchException;
    }

    protected function excludedRoutes($request)
    {
        $routes = [
            'api/v1/*',
        ];

        foreach ($routes as $route) {
            if ($request->is($route))
                return true;
        }

        return false;
    }

}
