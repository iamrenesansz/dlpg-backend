<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::controllers([
//     'auth' => 'Auth\AuthController',
//     'password' => 'Auth\PasswordController',
// ]);

// Pages
Route::get('/', ['as' => 'get.home', 'uses' => 'PagesController@index']);

// Admin
Route::group(array('prefix' => 'admin'), function() {
    
    // Main Page
    Route::get('/', ['as' => 'get.admin', 'uses' => 'AdminController@index']);
    // Auth
    Route::get('/login', ['as' => 'get.admin.login', 'uses' => 'AuthController@showLogin']); // Login Page
    Route::post('/login', ['as' => 'post.admin.dologin', 'uses' => 'AuthController@doLogin']); // Login Method
    Route::get('/logout', ['as' => 'get.admin.dologout', 'uses' => 'AuthController@doLogout']); // Login Page

});

// licenses
Route::group(array('prefix' => 'admin/licenses'), function() {
    
    Route::get('/',           ['as' => 'get.license',        'uses' => 'License\LicenseController@index']);
    Route::get('create',      ['as' => 'get.license.create', 'uses' => 'License\LicenseController@create']);
    Route::post('create',     ['as' => 'post.license.create', 'uses' => 'License\LicenseController@doCreate']);
    
    Route::get('edit/{id}',   ['as' => 'get.license.edit',   'uses' => 'License\LicenseController@edit']);
    Route::put('edit/{id}',   ['as' => 'put.license.update',   'uses' => 'License\LicenseController@doUpdate']);
    
    Route::get('delete/{id}', ['as' => 'delete.license',     'uses' => 'License\LicenseController@destroy']);

});

// violations
Route::group(array('prefix' => 'admin/violations'), function() {
    
    Route::get('/',           ['as' => 'get.violation',        'uses' => 'Violation\ViolationController@index']);
    Route::get('create',      ['as' => 'get.violation.create', 'uses' => 'Violation\ViolationController@create']);
    Route::post('create',     ['as' => 'post.violation.create', 'uses' => 'Violation\ViolationController@doCreate']);
    
    Route::get('edit/{id}',   ['as' => 'get.violation.edit',   'uses' => 'Violation\ViolationController@edit']);
    Route::put('edit/{id}',   ['as' => 'put.violation.update',   'uses' => 'Violation\ViolationController@doUpdate']);
    
    Route::get('delete/{id}', ['as' => 'delete.violation',     'uses' => 'Violation\ViolationController@destroy']);

});

// violation categories
Route::group(array('prefix' => 'admin/violation-categories'), function() {
    
    Route::get('/',           ['as' => 'get.violation-category',        'uses' => 'ViolationCategory\ViolationCategoryController@index']);
    Route::get('create',      ['as' => 'get.violation-category.create', 'uses' => 'ViolationCategory\ViolationCategoryController@create']);
    Route::post('create',     ['as' => 'post.violation-category.create', 'uses' => 'ViolationCategory\ViolationCategoryController@doCreate']);
    
    Route::get('edit/{id}',   ['as' => 'get.violation-category.edit',   'uses' => 'ViolationCategory\ViolationCategoryController@edit']);
    Route::put('edit/{id}',   ['as' => 'put.violation-category.update',   'uses' => 'ViolationCategory\ViolationCategoryController@doUpdate']);
    
    Route::get('delete/{id}', ['as' => 'delete.violation-category',     'uses' => 'ViolationCategory\ViolationCategoryController@destroy']); 

});

// officer
Route::group(array('prefix' => 'admin/officers'), function() {
    
    Route::get('/',           ['as' => 'get.officer',        'uses' => 'Officer\OfficerController@index']);
    Route::get('create',      ['as' => 'get.officer.create', 'uses' => 'Officer\OfficerController@create']);
    Route::post('create',     ['as' => 'post.officer.create', 'uses' => 'Officer\OfficerController@doCreate']);
    
    Route::get('edit/{id}',   ['as' => 'get.officer.edit',   'uses' => 'Officer\OfficerController@edit']);
    Route::put('edit/{id}',   ['as' => 'put.officer.update',   'uses' => 'Officer\OfficerController@doUpdate']);
    
    Route::get('delete/{id}', ['as' => 'delete.officer',     'uses' => 'Officer\OfficerController@destroy']); 

});

/*
|--------------------------------------------------------------------------
| App API Routes
|--------------------------------------------------------------------------
|
| These are the API routes for the app.
| Edit/change as needed as long you know what you are doing.
|
*/

/***************
 * API Vesion 1
 ***************/
Route::group(array('prefix' => 'api/v1'), function() {

    // Auth
    Route::post('auth', ['as' => 'post.api.v1.auth', 'uses' => 'API\v1\AuthController@login']);

    // License
    Route::get('licenses/{id?}', ['as' => 'get.api.v1.license',  'uses' => 'API\v1\LicenseController@show']);
    Route::post('licenses',      ['as' => 'post.api.v1.license', 'uses' => 'API\v1\LicenseController@store']);
    Route::put('licenses/{id}',  ['as' => 'put.api.v1.license',  'uses' => 'API\v1\LicenseController@update']);

    // Violations
    Route::get('violations/{id?}', ['as' => 'get.api.v1.violations', 'uses' => 'API\v1\ViolationController@show']);

});
