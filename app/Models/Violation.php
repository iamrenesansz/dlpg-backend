<?php namespace DLPG\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Violation extends Eloquent {

    /**
     * define which attributes are mass assignable (for security)
     * we only want these 3 attributes able to be filled
     * 
     * @var array
     */
    protected $fillable = array('violation_cat_id', 'title', 'min_rate', 'max_rate');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_violations';

}