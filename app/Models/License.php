<?php namespace DLPG\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class License extends Eloquent {

    /**
     * define which attributes are mass assignable (for security)
     * we only want these 3 attributes able to be filled
     * 
     * @var array
     */
    protected $fillable = array('license_num', 'first_name', 'middle_name', 'last_name', 'address', 'conditions', 'agy', 'image', 'restrictions', 'birth_date', 'license_type', 'gender', 'height', 'weight', 'nationality', 'license_exp_date');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_licenses';

}