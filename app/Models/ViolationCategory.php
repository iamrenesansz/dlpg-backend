<?php namespace DLPG\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ViolationCategory extends Eloquent {

    /**
     * define which attributes are mass assignable (for security)
     * we only want these 3 attributes able to be filled
     * 
     * @var array
     */
    protected $fillable = array('title');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_violation_categories';

}