<?php namespace DLPG\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class OfficerDevice extends Eloquent {

    /**
     * define which attributes are mass assignable (for security)
     * we only want these 3 attributes able to be filled
     * 
     * @var array
     */
    protected $fillable = array('officer_id', 'device_id');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_officer_devices';

}