@extends('app')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h1>Add License</h1></div>
            <div class="panel-body">
                <form method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-4">  
                          <div class="form-group">
                            <label for="license_num">License Number</label>
                            <input type="text" class="form-control" name="license_num" required>
                          </div>
                        </div>
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="license_type">License Type</label>
                            <select class="form-control" name="license_type">
                              <option value="Non-Professional">Non-Professional</option>
                              <option value="Professional">Professional</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="license_exp_date">License Expiry Date</label>
                            <input type="text" class="form-control" name="license_exp_date" required>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">  
                          <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" name="first_name" required>
                          </div>
                        </div>
                        <div class="col-md-4">  
                          <div class="form-group">
                            <label for="middle_name">Middle Name</label>
                            <input type="text" class="form-control" name="middle_name" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control" name="last_name" required>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">  
                          <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" name="address" required>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="birth_date">Birth Date</label>
                            <input type="text" class="form-control" name="birth_date" required>
                          </div>
                        </div>
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="nationality">Nationality</label>
                            <input type="text" class="form-control" name="nationality" required>
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="gender">Gender</label>
                            <select class="form-control" name="gender">
                              <option value="M">M</option>
                              <option value="F">F</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="height">Height</label>
                            <input type="text" class="form-control" name="height" required>
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="weight">Weight</label>
                            <input type="text" class="form-control" name="weight" required>
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="conditions">Conditions</label>
                            <input type="text" class="form-control" name="conditions" required>
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="restrictions">Retrisctions</label>
                            <input type="text" class="form-control" name="restrictions" required>
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="agy">AGY</label>
                            <input type="text" class="form-control" name="agy" required>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" class="form-control" name="image" required>
                          </div>
                        </div>
                    </div>
                  <button type="submit" class="btn btn-primary">Add</button>
                  <a href="{{ route('get.license') }}" class="btn btn-default">Back</a>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection