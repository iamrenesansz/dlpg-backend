@extends('app')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h1>Licenses</h1>
                <a href="{{ route('get.license.create') }}">
                    <button class="btn btn-success"><i class="fa fa-plus"></i> Add</button>
                </a>
                <form method="GET" class="form-inline">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="sr-only" for="query">Name or License #</label>
                        <input type="text" class="form-control" name="query" placeholder="Name or License #">
                    </div>
                    <button type="submit" class="btn btn-default">Search</button>
                </form>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>License #</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Birth Date</th>
                        <th>License Type</th>
                        <th>Gender</th>
                        <th>Restrictions</th>
                        <th>Nationality</th>
                        <th>License Exp. Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($results as $license )
                        <tr>
                            <td>
                                @if (!is_null($license['image']))
                                    <img src="../storage/{{ $license['image'] }}" height="80px" width="80px">
                                @endif
                            </td>
                            <td>{{ $license['license_num'] }}</td>
                            <td>{{ $license['first_name'] }}</td>
                            <td>{{ $license['middle_name'] }}</td>
                            <td>{{ $license['last_name'] }}</td>
                            <td>{{ $license['address'] }}</td>
                            <td>{{ $license['birth_date'] }}</td>
                            <td>{{ $license['license_type'] }}</td>
                            <td>{{ $license['gender'] }}</td>
                            <td>{{ $license['restrictions'] }}</td>
                            <td>{{ $license['nationality'] }}</td>
                            <td>{{ $license['license_exp_date'] }}</td>
                            <td><a href="{{ route('get.license.edit', ['id' => $license['id']]) }}"><button class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></button></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection