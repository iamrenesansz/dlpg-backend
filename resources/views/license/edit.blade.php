@extends('app')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h1>Edit License</h1></div>
            <div class="panel-body">
            @if ($updated)
                <div class="alert alert-success" role="alert">Updated!</div>
            @endif
                <form method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-4">  
                          <div class="form-group">
                            <label for="input[license_num]">License Number</label>
                            <input type="text" class="form-control" name="input[license_num]" value="{{ $results['license_num'] }}" required>
                          </div>
                        </div>
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="input[license_type]">License Type</label>
                            <input type="text" class="form-control" name="input[license_type]" value="{{ $results['license_type'] }}" required>
                          </div>
                        </div>
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="input[license_exp_date]">License Expiry Date</label>
                            <input type="text" class="form-control" name="input[license_exp_date]" value="{{ $results['license_exp_date'] }}" required>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">  
                          <div class="form-group">
                            <label for="input[first_name]">First Name</label>
                            <input type="text" class="form-control" name="input[first_name]" value="{{ $results['first_name'] }}" required>
                          </div>
                        </div>
                        <div class="col-md-4">  
                          <div class="form-group">
                            <label for="input[middle_name]">Middle Name</label>
                            <input type="text" class="form-control" name="input[middle_name]" value="{{ $results['middle_name'] }}" required>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="input[last_name]">Last Name</label>
                            <input type="text" class="form-control" name="input[last_name]" value="{{ $results['last_name'] }}" required>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">  
                          <div class="form-group">
                            <label for="input[address]">Address</label>
                            <input type="text" class="form-control" name="input[address]" value="{{ $results['address'] }}" required>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="input[birth_date]">Birth Date</label>
                            <input type="text" class="form-control" name="input[birth_date]" value="{{ $results['birth_date'] }}" required>
                          </div>
                        </div>
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="input[nationality]">Nationality</label>
                            <input type="text" class="form-control" name="input[nationality]" value="{{ $results['nationality'] }}" required>
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="input[gender]">Gender</label>
                            <input type="text" class="form-control" name="input[gender]" value="{{ $results['gender'] }}" required>
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="input[height]">Height</label>
                            <input type="text" class="form-control" name="input[height]" value="{{ $results['height'] }}" required>
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="input[weight]">Weight</label>
                            <input type="text" class="form-control" name="input[weight]" value="{{ $results['weight'] }}" required>
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="input[conditions]">Conditions</label>
                            <input type="text" class="form-control" name="input[conditions]" value="{{ $results['conditions'] }}">
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="input[restrictions]">Restrictions</label>
                            <input type="text" class="form-control" name="input[restrictions]" value="{{ $results['restrictions'] }}">
                          </div>
                        </div>
                        <div class="col-md-1">  
                          <div class="form-group">
                            <label for="input[agy]">AGY</label>
                            <input type="text" class="form-control" name="input[agy]" value="{{ $results['agy'] }}">
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        @if (!is_null($results['image']))
                          <div class="col-md-2">  
                            <img src="../../../storage/{{ $results['image'] }}" height="150px" width="150px">
                          </div>
                        @endif
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" class="form-control" name="image">
                          </div>
                        </div>
                    </div><br>
                  <button type="submit" class="btn btn-primary">Update</button>
                  <a href="{{ route('get.license') }}" class="btn btn-default">Back</a>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection