<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>DLPG</title>

	<link href="/css/app.css" rel="stylesheet">
	<link href="/bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	<link href="/bower_components/bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">
	<link href="/bower_components/fontawesome/css/font-awesome.css" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">DLPG</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				@if (Auth::guest())
						<!-- <li><a href="/auth/login">Login</a></li>
						<li><a href="/auth/register">Register</a></li> -->
				@else
				<ul class="nav navbar-nav">
					<li><a href="{{ route('get.admin') }}">Dashboard</a></li>
					<li><a href="{{ route('get.license') }}">Licenses</a></li>
					<li><a href="{{ route('get.violation') }}">Violations</a></li>
					<li><a href="{{ route('get.violation-category') }}">Violation Categories</a></li>
					<li><a href="{{ route('get.officer') }}">Officers</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="/admin/logout">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>
	
	<div class="container">	
		@yield('content')
	</div>

	<!-- Scripts -->
	<script src="/bower_components/jquery/dist/jquery.js"></script>
	<script src="/bower_components/bootstrap/dist/js/bootstrap.js"></script>
</body>
</html>
