@extends('app')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h1>Violation Category</h1> <a href="{{ route('get.violation-category.create') }}"><button class="btn btn-success"><i class="fa fa-plus"></i> Add</button></a>
            <form method="GET" class="form-inline">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="sr-only" for="query">Title</label>
                        <input type="text" class="form-control" name="query" placeholder="Title">
                    </div>
                    <button type="submit" class="btn btn-default">Search</button>
                </form>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="col-md-11">Tite</th>
                        <th class="col-md-1">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($results as $violationCategory )
                        <tr>
                            <td>{{ $violationCategory['title'] }}</td>
                            <td><a href="{{ route('get.violation-category.edit', ['id' => $violationCategory['id']]) }}"><button class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></button></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection