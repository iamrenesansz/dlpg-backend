@extends('app')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h1>Add Violation</h1></div>
            <div class="panel-body">
                <form method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-6">  
                          <div class="form-group">
                            <label for="violation_cat_id">Category</label>
                            <select class="form-control" name="violation_cat_id">
                              @foreach ($violationCategories as $category)
                                <option value="{{ $category['id'] }}">{{ $category['title'] }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">  
                          <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" required>
                          </div>
                        </div>
                    </div>
                  <button type="submit" class="btn btn-primary">Add</button>
                  <a href="{{ route('get.violation') }}" class="btn btn-default">Back</a>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection