@extends('app')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h1>Edit Violation</h1></div>
            <div class="panel-body">
            @if ($updated)
                <div class="alert alert-success" role="alert">Updated!</div>
            @endif
                <form method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-6">  
                          <div class="form-group">
                            <label for="violation_cat_id">Category</label>
                            <select class="form-control" name="input[violation_cat_id]">
                              @foreach ($violationCategories as $category)
                                @if ($category['id'] === $results['violation_cat_id'])
                                  <option value="{{ $category['id'] }}" selected>{{ $category['title'] }}</option>
                                @else
                                  <option value="{{ $category['id'] }}">{{ $category['title'] }}</option>
                                @endif
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">  
                          <div class="form-group">
                            <label for="input[title]">Title</label>
                            <input type="text" class="form-control" name="input[title]" value="{{ $results['title'] }}" required>
                          </div>
                        </div>
                    </div>
                  <button type="submit" class="btn btn-primary">Update</button>
                  <a href="{{ route('get.violation') }}" class="btn btn-default">Back</a>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection