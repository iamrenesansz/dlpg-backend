@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h1>License Violations</h1>
					<form method="GET" class="form-inline">
	                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					 	<div class="form-group">
					    	<label class="sr-only" for="query">Name or License #</label>
					    	<input type="text" class="form-control" name="query" placeholder="Name or License #">
					 	</div>
  						<button type="submit" class="btn btn-default">Search</button>
					</form>
				</div>
	            <table class="table table-bordered">
	                <thead>
	                    <tr>
	                        <th></th>
	                        <th>License #</th>
	                        <th>Name</th>
	                        <th>Violation</th>
	                        <th>Violation Date</th>
	                    </tr>
	                </thead>
	                <tbody>
	                    @foreach ($results as $license )
	                        <tr>
	                            <td>
	                            	@if (!is_null($license['image']))
		                            	<img src="../storage/{{ $license['image'] }}" height="80px" width="80px">
	                            	@endif
	                            </td>
	                            <td>{{ $license['license_num'] }}</td>
	                            <td>{{ $license['name'] }}</td>
	                            <td>{{ $license['violation'] }}</td>
	                            <td>{{ $license['created_at'] }}</td>
	                        </tr>
	                    @endforeach
	                </tbody>
	            </table>
			</div>
		</div>
	</div>
</div>
@endsection
