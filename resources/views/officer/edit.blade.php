@extends('app')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h1>Edit Officer</h1></div>
            <div class="panel-body">
            @if ($updated)
                <div class="alert alert-success" role="alert">Updated!</div>
            @endif
                <form method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-4">  
                          <div class="form-group">
                            <label for="input[control_num]">Control Number</label>
                            <input type="text" class="form-control" name="input[control_num]" value="{{ $results['control_num'] }}" required>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">  
                          <div class="form-group">
                            <label for="input[first_name]">First Name</label>
                            <input type="text" class="form-control" name="input[first_name]" value="{{ $results['first_name'] }}" required>
                          </div>
                        </div>
                      
                        <div class="col-md-6">  
                          <div class="form-group">
                            <label for="input[last_name]">Last Name</label>
                            <input type="text" class="form-control" name="input[last_name]" value="{{ $results['last_name'] }}" required>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">  
                          <div class="form-group">
                            <label for="input[address]">Address</label>
                            <input type="text" class="form-control" name="input[address]" value="{{ $results['address'] }}" required>
                          </div>
                        </div>
                      
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="input[contact_num]">Contct Number</label>
                            <input type="text" class="form-control" name="input[contact_num]" value="{{ $results['contact_num'] }}" required>
                          </div>
                        </div>
                    </div>

                  <button type="submit" class="btn btn-primary">Update</button>
                  <a href="{{ route('get.officer') }}" class="btn btn-default">Back</a>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection