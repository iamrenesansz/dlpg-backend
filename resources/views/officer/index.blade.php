@extends('app')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h1>Officers</h1> <a href="{{ route('get.officer.create') }}"><button class="btn btn-success"><i class="fa fa-plus"></i> Add</button></a>

                <form method="GET" class="form-inline">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label class="sr-only" for="query">Name or Control #</label>
                        <input type="text" class="form-control" name="query" placeholder="Name or Control #">
                    </div>
                    <button type="submit" class="btn btn-default">Search</button>
                </form>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Control #</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Contact Num</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($results as $officer )
                        <tr>
                            <td>{{ $officer['control_num'] }}</td>
                            <td>{{ $officer['first_name'] }}</td>
                            <td>{{ $officer['last_name'] }}</td>
                            <td>{{ $officer['address'] }}</td>
                            <td>{{ $officer['contact_num'] }}</td>
                            <td><a href="{{ route('get.officer.edit', ['id' => $officer['id']]) }}"><button class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></button></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection