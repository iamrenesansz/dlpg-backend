@extends('app')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><h1>Add Officer</h1></div>
            <div class="panel-body">
                <form method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-4">  
                          <div class="form-group">
                            <label for="control_num">Control Number</label>
                            <input type="text" class="form-control" name="control_num" required>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">  
                          <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" name="first_name" required>
                          </div>
                        </div>
                      
                        <div class="col-md-6">  
                          <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control" name="last_name" required>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10">  
                          <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" name="address" required>
                          </div>
                        </div>
                      
                        <div class="col-md-2">  
                          <div class="form-group">
                            <label for="contact_num">Contct Number</label>
                            <input type="text" class="form-control" name="contact_num" required>
                          </div>
                        </div>
                    </div>
                  <button type="submit" class="btn btn-primary">Add</button>
                  <a href="{{ route('get.officer') }}" class="btn btn-default">Back</a>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection